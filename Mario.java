
import java.util.Scanner;

class Mario{
    
    public static void main(String []argv){
	int stairs = input();
	drawStairs(stairs);
    }
    /*
    function for check (string == int)
    @param string
    @return true/false
    */
    public static boolean isInt(String str){
	int num;
	try {
	    num = Integer.parseInt(str);
	} catch(NumberFormatException e) {
	    System.out.println("Don`t correct format!!!");
	    return false;
	} catch(NullPointerException e) {
	    System.out.println("Empty string!!!");
	    return false;
	}
	if(num < 1 || num > 23){
	    System.out.println("Your num mus`t be more then 0 and less then 24!!!");
	    return false;
	}
	return true;
    }
    /*
    function for input data
    @param
    @return correct data
    */
    public static int input(){
	Scanner in = new Scanner(System.in);
	String str;
	do{
	    System.out.println("Input count stairs:");
	    str = in.nextLine();
	}while(!isInt(str));
	return Integer.parseInt(str);
    }
    /*
    function for draw stairs on screen
    @param count stairs
    @return output stairs on screen
    */
    public static void drawStairs(int stairs){
	for(int i = 0; i < stairs; i++){
	    for(int j = 0; j < stairs + 1; j++){
		if(stairs - i -2 < j){
		    System.out.printf("#");
		} else {
		    System.out.printf(" ");
		}
	    }
	    System.out.println();
	}

    }
}